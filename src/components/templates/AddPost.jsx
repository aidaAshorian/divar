import { useRef, useState } from "react";
import {useQuery, useQueryClient } from "@tanstack/react-query";
import axios from "axios";
import { toast } from 'react-toastify';
import { getCategory } from "src/services/admin";
import styles from './AddPost.module.css';
import { getCookie } from "src/utils/cookie";
import { p2e,e2p } from "src/utils/number";


function AddPost() {
    const [form,setForm] = useState({
        title:"",
        content:"",
        city:"",
        category:"",
        amount:null,
        images:null,
    });
       const baseUrl = import.meta.env.VITE_BASE_URL;
       const {data} = useQuery(['get-categories'] , getCategory);
       const queryClient = useQueryClient();
       const formInput = useRef(null);

       const checkHandler = (event) => {
        let regex=new RegExp(/(۰|۱|۲|۳|۴|۵|۶|۷|۸|۹)$/);
        if (regex.test(event.target.value)){
            event.target.value = p2e(event.target.value)
        }
       }
    const addHandler = (event) =>{
        event.preventDefault();
        const formData = new FormData();
        for(let i in form ){
            formData.append(i,form[i])
        }
        const token = getCookie('accessToken');
        axios.post(`${baseUrl}post/create`,formData,{
            headers:{
                'Content-Type':' multipart/form-data',
                Authorization :`bearer ${token}`,
            }
        })
        .then((res) => {queryClient.invalidateQueries('my-post-list'),toast.success(res.data.message),
        (res.data) ? formInput.current.reset() : null})
        .catch((error) => toast.error('مشکلی پیش آمده است'));
    };

        const changeHandler =(event)=>{
        event.preventDefault();
        const name = event.target.name;
        if(name !== "images"){
            setForm({...form,[name]:event.target.value});
        }else{
            setForm({...form,[name]:event.target.files[0]});
        }
       };
       console.log(form);
    return (
      <form ref={formInput} onChange={changeHandler} className={styles.form}>
        <h3>افزودن آگهی</h3>
        <label htmlFor="title">عنوان</label>
        <input type="text" name="title" id="title"/>
        <label htmlFor="content">توضیحات</label>
        <textarea name="content" id="content" />
        <label htmlFor="amount">قیمت</label>
        <input type="text" name="amount" id="amount" onChange={checkHandler} />
        <label htmlFor="city">شهر</label>
        <input type="text" name="city" id="city" />
        <label htmlFor="category">دسته بندی</label>
        <select name="category" id="category">
            {data?.data.map((item) =>(
            <option key={item._id} value={item._id}>
                {item.name}
            </option>
            ))}
        </select>
        <label htmlFor="images">عکس</label>
        <input type="file" name="images" id="images" />
        <button onClick={addHandler}>ایجاد</button>
      </form>
    );
}

export default AddPost;