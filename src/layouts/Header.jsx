import { useState } from "react";
import { Link } from "react-router-dom";
import styles from './Header.module.css';
import Menu from "./Menu";
import CityList from "src/components/templates/city/CityList";

function Header() {
    const [visible , setVisible] = useState(false);
    const [open , setOpen] = useState(false);

    const clickHandler = ()=>{
        setVisible(!visible)
        console.log(visible)
    };
    const clickOpenHandler = () =>{
        setOpen(!open);
    };
    return (
       <header className={styles.header}>
        <div>
            <Link to="/">
                <img src='divar.svg' className={styles.logo}/>
            </Link>
            <span open={open} onClick={clickOpenHandler}>
                <img src='location.svg'/>
                <p>تهران</p>
                { open ? <CityList open={open}/> : null}
            </span>
        </div>
        <div>
              <div onClick={clickHandler} style={{position:'relative'}}>
                   <span>
                   <img src='profile.svg'/>
                  <p>دیوار من</p>
                   </span>
                  { visible ? <Menu/> : null}
               </div>
            <Link to="/dashboard" className={styles.button}>
                ثبت آگهی
            </Link>
        </div>
       </header>
    );
}

export default Header;