import { QueryClient,QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { ToastContainer } from "react-toastify";
import  defaultOptions  from "src/configs/reactQuery";
import Router from "src/router/Router";
import { BrowserRouter } from "react-router-dom";
import Layout from "./layouts/Layout";


function App() {

const queryClient = new QueryClient({defaultOptions});

  return(
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
      <Layout>
          <Router/>
          <ToastContainer/>
      </Layout>
      </BrowserRouter>
      <ReactQueryDevtools/>
   </QueryClientProvider>
)}

export default App;
