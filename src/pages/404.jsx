
function PageNotFound() {
    return (
        <div style={{width:'100%',display:'flex',marginTop:'5rem',alignItems:'center',justifyContent:'center'}}>
            <img src="404.svg" style={{width:'20rem'}} />
        </div>
    );
}

export default PageNotFound;