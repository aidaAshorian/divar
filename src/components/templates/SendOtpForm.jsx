import { useEffect, useState } from "react";
import { SendOtp } from "../../services/auth";
import { mobileValidation } from "../../validation/validationOtp";
import styles from './SendOtpForm.module.css';

function SendOtpForm({mobile,setMobile,setStep}) {

    const [touched,setTouched] = useState(false);
    const [errorMobile,setErrorMobile] = useState('');

    useEffect(()=>{
        setErrorMobile(mobileValidation(mobile))
    },[touched,mobile]);

    const submitHandler= async(event)=>{
        event.preventDefault();
        const {response,error} = await SendOtp(mobile);
        console.log({response,error});
        if (response) setStep(2);
        if (error) console.log(error.response.data.message);
    }
    return (
        <form onSubmit={submitHandler} className={styles.form}>
            <p>ورود به حساب کاربری</p>
            <span>برای استفاده از امکانات دیوار، لطفاً شمارهٔ موبایل خود را وارد کنید. کد تأیید به این شماره پیامک خواهد شد.</span>
            <label htmlFor="input">شماره موبایل</label>
            <input type="text" id="input" placeholder="شماره موبایل" value={mobile} onFocus={()=>setTouched(true)} onChange={(e)=>setMobile(e.target.value)} autoFocus/>
            {errorMobile && touched && <span style={{color:'red',fontSize:'12px'}}>{errorMobile}</span>}
            <p><span className={styles.span} > شرایط استفاده از خدمات</span> و <span className={styles.span}>حریم خصوصی</span> دیوار را می‌پذیرم.</p>
            <button type="submit">تایید</button>
        </form>
    );
}

export default SendOtpForm;