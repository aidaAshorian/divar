import { useQuery } from "@tanstack/react-query";
import { Link, useParams } from "react-router-dom";
import { getAllPosts } from "src/services/user";
import styles from './ProductDetails.module.css'
import { sp } from "src/utils/number";

function ProductDetails() {
    const {id} = useParams();
    const {data, isLoading} = useQuery(['post-list'] , getAllPosts);
    const baseURL = import.meta.env.VITE_BASE_URL;
    console.log(data)
    const post =  data?.data.posts?.filter((post) => post._id === id);
    console.log(post)
    return (
        <div className={styles.container}>
            {isLoading ?
             (<loader/>) :(
             <>
             <img className={styles.image} src={`${baseURL}${post[0].images[0]}`} />
             <div className={styles.textContainer}>
               <p>{sp(post[0].amount)} تومان</p>
               <h3>{post[0].options.title}</h3>
                <p className={styles.description}>{post[0].options.content}</p>
                <p className={styles.category}>{post[0].options.city}</p>
                <div className={styles.buttonContainer}>
                <button > اطلاعات تماس </button>
                <Link to="/" >بازگشت به صفحه اصلی</Link>
                </div>
             </div>
            </>
       ) }
        </div>
    );
}

export default ProductDetails;