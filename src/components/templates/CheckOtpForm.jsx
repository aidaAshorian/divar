import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import { CheckOtp } from "../../services/auth";
import { setCookie } from 'src/utils/cookie';
import { getProfile } from 'src/services/user';
import styles from './CheckOtpForm.module.css';


function CheckOtpForm({code,setCode, mobile, setStep}) {
    const navigate = useNavigate();
    const {refetch} = useQuery(['profile'],getProfile);

    const submitHandler=async(event)=>{
        event.preventDefault();
        console.log({mobile,code});
        const {response,error} =await CheckOtp(mobile,code);
        if (response) {
            toast.success('شما با موفقیت وارد شدید',{position:'top-left'});
            setCookie(response.data);
            navigate('/dashboard');
            refetch();
        }
        if(error){
            toast.warn('کد تایید معتبر نیست.',{position:'top-left'});
            console.log(error.response.data.message);
        } 
    }
    return (
        <form onSubmit={submitHandler} className={styles.form}>
            <p>کد تأیید را وارد کنید</p>
            <span>کد پیامک‌شده به شمارۀ «{mobile}» را وارد کنید.</span>
            <label htmlFor="input">کد تأیید را وارد کنید</label>
            <input type="text" id="input" value={code} placeholder="کد تأیید" onChange={(e) => setCode(e.target.value)} autoFocus />
            <button className={styles.blackButton} onClick={()=>setStep(1)}>تغییر شمارهٔ موبایل</button>
            <button type="submit">ورود</button>
        </form>
    );
}

export default CheckOtpForm;