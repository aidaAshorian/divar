const mobileValidation = (mobile) =>{
    const iranianMobilePattern = /^09[0|1|2|3][0-9]{8}$/;
    var errorMobile = '';
    if (mobile.length !== 11 || !iranianMobilePattern.test(mobile)){
        errorMobile = 'لطفا یک شماره موبایل معتبر وارد نمایید'; 
    }else{
        errorMobile='';
    }
   return errorMobile;
}
export {mobileValidation};