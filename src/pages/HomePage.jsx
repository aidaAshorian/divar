import { useQuery } from "@tanstack/react-query";
import Main from "src/components/templates/Main";
import Sidebar from "src/components/templates/Sidebar";
import MainFilterCategory from "src/components/templates/MainFilterCategory";
import { getCategory } from "src/services/admin";
import { getAllPosts } from "src/services/user";
import Loader from "src/components/modules/Loader";
import { useParams } from "react-router-dom";
import styles from './HomePage.module.css';

function HomePage() {
    const {id} = useParams();
    const {data:categories, isLoading:categoryLoading} = useQuery(['get-categories'] , getCategory);
    const {data:posts, isLoading:postsLoading} = useQuery(['post-list'] , getAllPosts);
    const postFilter =  posts?.data.posts?.filter((post) => post.category === id);
    console.log(postFilter);
    console.log(posts);
    console.log({id});

    return (
        <>
        {postsLoading || categoryLoading ?(
            <Loader/>
        ):(
            <div className={styles.home}>
                <Sidebar categories={categories}/>
                { id ? (
                    <MainFilterCategory posts={postFilter}/>
                ):(
                    <Main posts={posts}/>
                )}
            </div>
        )}
        </>
    );
}

export default HomePage;