import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import styles from './PostList.module.css';
import { getPosts } from 'src/services/user';
import Loader from '../modules/Loader';
import { sp } from 'src/utils/number';
import { deletedPost } from 'src/services/user';

function PostList() {
    const queryClient = useQueryClient();
    const {data,isLoading}= useQuery(['my-post-list'],getPosts);
    console.log(data);
    const baseURL = import.meta.env.VITE_BASE_URL;

    const {mutate} = useMutation(deletedPost,{
        onSuccess:()=>queryClient.invalidateQueries('get-categories'),
    });
    const deleteHandler =(event)=>{
        event.preventDefault();
        const id = event.target.id;
        console.log(id);
        mutate(id)
    };
    return (
        <div className={styles.list}>
            {
                isLoading ? (
                <Loader/>
                ) : (
                    <>
                    <h3>آگهی های شما</h3>
                    {
                    data.data.posts?.map ( post => (
                        <div key={post._id} className={styles.post}>
                            <img src={`${baseURL}${post.images[0]}`} />
                            <div>
                                <p>{post.options?.title}</p>
                                <span>{post.options?.content}</span>
                            </div>
                            <div className={styles.price}>
                                <p>{new Date(post.createdAt).toLocaleDateString("fa-IR")}</p>
                                <span>{sp(post.amount)} تومان</span>
                            </div>
                            <button id={post._id} onClick={deleteHandler}>حذف آگهی</button>
                        </div>
                    ))}
                    </>
                )
            }
        </div>
    );
}

export default PostList;