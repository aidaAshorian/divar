import { useQuery } from '@tanstack/react-query';
import { Link } from 'react-router-dom';
import styles from './Menu.module.css'
import { getProfile } from 'src/services/user';
import { deleteCookie } from 'src/utils/cookie';

function Menu() {
    const {data} = useQuery(['profile'],getProfile);

    const clickHandler = (event) =>{
        event.preventDefault();
        deleteCookie('accessToken');
        deleteCookie('refreshToken');
        location.reload();
    }
    return (
            <div className={styles.main}>
                    <div>
                        <p><img className={styles.img} src='profile.svg'/>
                        {!data && <Link to="/auth"> کاربر دیوار </Link>}
                        {data.data?.role === "USER" && <Link to="/dashboard">   کاربر دیوار </Link>}
                        {data.data?.role === "ADMIN" && <Link to="/admin"> ادمین دیوار </Link>}
                         </p>
                        <span>{data.data?.mobile}</span>
                    </div>
                    {!data && 
                      <div>
                            <Link to="/auth">
                              <p>
                                <img className={styles.img} src='confirmation.svg'/>
                                  تایید هویت
                             </p>
                            </Link>
                       </div>
                    
                    }
                { data &&
                   <div onClick={clickHandler}>
                      <Link to="/">
                        <p><img className={styles.img} src='signout.svg'/>خروج</p>
                      </Link> 
                   </div>
                 }
                </div>
    );
}

export default Menu;