import { Link } from 'react-router-dom';
import styles from './Sidebar.module.css';

function Sidebar({categories}) {
    return (
        <div className={styles.sidebar}>
            <h4>دسته ها</h4>
            <ul>
                {categories.data.map((category) =>(
                    <Link key={category._id} to={`/${category._id}`}> 
                       <li >
                          <img src={`${category.icon}.svg`} />
                          <p>{category.name}</p>
                        </li>
                    </Link>
                ))}
            </ul>
        </div>
    );
}

export default Sidebar;