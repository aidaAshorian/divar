import { useQuery,useMutation, useQueryClient } from "@tanstack/react-query";
import { getCategory,deleteCategory } from "src/services/admin";
import Loader from "components/modules/Loader";
import styles from './CategoryList.module.css';


function CategoryList() {
    const queryClient = useQueryClient();
    const {data, isLoading} = useQuery(['get-categories'] , getCategory);
    
    const {mutate} = useMutation(deleteCategory,{
        onSuccess:()=>queryClient.invalidateQueries('get-categories'),
    });

    const deleteHandler = (event) =>{
        event.preventDefault();
        const id = event.target.id;
        mutate(id)
    };
    return (
        <div  className={styles.list}>
            {isLoading ? (<Loader/>) :
            (data.data.map((item) =>(
                <div key={item._id}>
                    <img src={`${item.icon}.svg`}/>
                    <h5>{item.name}</h5>
                    <p>slug : {item.slug}</p>
                    <button id={item._id} onClick={deleteHandler}>حذف</button>
                </div>
            )))
            }
        </div>
    );
}

export default CategoryList;