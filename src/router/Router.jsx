import { useQuery } from "@tanstack/react-query";
import { Route, Routes,Navigate } from "react-router-dom";
import { getProfile } from "../services/user";

import HomePage from "src/pages/HomePage";
import DashboardPage from "src/pages/DashboardPage";
import AuthPage from "src/pages/AuthPage";
import AdminPage from "src/pages/AdminPage";
import PageNotFound from "src/pages/404";
import Loader from "src/components/modules/Loader";
import ProductDetails from "src/components/templates/ProductDetails";

function Router() {
    const {data,isLoading} = useQuery(['profile'],getProfile);
    if(isLoading) return  <Loader/>;
    return (
        <Routes>
            <Route path='/productDetails/:id' element={<ProductDetails/>} />
            <Route path="/:id?" element={<HomePage/>}/>
            <Route path="/dashboard" element={data ? <DashboardPage/> : <Navigate to="/auth"/>}/>
            <Route path="/auth" element={data ? <Navigate to="/dashboard"/> : <AuthPage/>}/>
            <Route path="/admin" element={data && data.data.role === 'ADMIN' ? <AdminPage/> : <Navigate to="/"/>}/>
            <Route path="/notFound" element={<PageNotFound/>}/>
            <Route path="*" element={<Navigate to='/notFound'/>}/>
        </Routes>
    );
}

export default Router;